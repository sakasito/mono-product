import 'package:flutter/material.dart';
import 'package:mono_product/screen/detail_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Gun Shop',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const DetailPage(title: 'Gun Shop'),
    );
  }
}
