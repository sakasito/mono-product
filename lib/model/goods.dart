import 'package:flutter/material.dart';

class Goods {
  static BuildContext? context;
  late final String? name;
  final String? thumbImageURL;
  final double? price;
  final String? description;

  Goods({
    this.name,
    this.thumbImageURL,
    this.price,
    this.description,
  });

  factory Goods.fromJson(Map<String, dynamic> json) {
    return Goods(
      name: json['name'],
      thumbImageURL: json['thumbImageURL'],
      price: json['price'],
      description: json['description'],
    );
  }

}
