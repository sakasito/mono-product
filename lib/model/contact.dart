import 'package:flutter/material.dart';

class Contact {
  static BuildContext? context;
  late final String? name;
  late final String? phone;
  late final String? address;

  Contact({
    this.name,
    this.phone,
    this.address,
  });

  factory Contact.fromJson(Map<String, dynamic> json) {
    return Contact(
      name: json['name'],
      phone: json['phone'],
      address: json['address'],
    );
  }

}
