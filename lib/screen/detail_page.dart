import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mono_product/model/goods.dart';
import 'package:mono_product/screen/contact_page.dart';
import 'dart:convert';

class DetailPage extends StatefulWidget {
  const DetailPage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  State<DetailPage> createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  late int counter = 1;
  late bool isLoading = true;
  late Goods good =
      Goods(name: '', price: 0, description: '', thumbImageURL: '');
  @override
  void initState() {
    setGoods();
    super.initState();
  }
  // JSON example that will be used to to assign data
  Future<void> setGoods() async {
    String response = await rootBundle.loadString('assets/json/goods.json');
    Goods gos = Goods.fromJson(jsonDecode(response));
    good = Goods(
        name: gos.name,
        price: gos.price,
        description: gos.description,
        thumbImageURL: gos.thumbImageURL);
    setState(() {
      isLoading = false;
    });
  }

  void redirectToContact(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => ContactPage(goods: good, amount: counter)),
    );
  }

  void showBottomModel() {
    showModalBottomSheet<dynamic>(
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter mystate) {
            return Padding(
                padding: const EdgeInsets.all(10),
                child: Wrap(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 16),
                              child: const Text('จำนวน'),
                            ),
                            Row(
                              children: <Widget>[
                                Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 16),
                                    child: ElevatedButton(  
                                      onPressed: () {
                                        if (counter > 1) {
                                          mystate(() {
                                            counter -= 1;
                                          });
                                        }
                                      },
                                      child: const Text(
                                        '-',
                                        style: TextStyle(fontSize: 25),
                                      ),
                                      style: ElevatedButton.styleFrom(
                                        shape: const CircleBorder(),
                                        padding: const EdgeInsets.all(15),
                                      ),
                                    )),
                                Text('$counter'),
                                Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 16),
                                    child: ElevatedButton(
                                      onPressed: () {
                                        mystate(() {
                                          counter += 1;
                                        });
                                      },
                                      child: const Text(
                                        '+',
                                        style: TextStyle(fontSize: 25),
                                      ),
                                      style: ElevatedButton.styleFrom(
                                        shape: const CircleBorder(),
                                        padding: const EdgeInsets.all(15),
                                      ),
                                    ))
                              ],
                            ),
                          ],
                        ),
                        Align(
                          alignment: Alignment.centerRight,
                          child: Container(
                            padding: const EdgeInsets.fromLTRB(0, 20, 20, 20),
                            child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 16),
                                shape: const RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(2)),
                                ),
                              ),
                              onPressed: () => redirectToContact(context),
                              child: const Text('ยืนยัน'),
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ));
          });
        });
  }

  @override
  Widget build(BuildContext context) {
    // setGoods();
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(widget.title),
      ),
      body: SingleChildScrollView(
        child: isLoading
            ? null
            : Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(good.thumbImageURL!),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.fromLTRB(10, 20, 10, 0),
                        child: Text('ราคา: ${good.price!} บาท'),
                      ),
                      Container(
                        padding: const EdgeInsets.fromLTRB(0, 20, 10, 0),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            shape: const RoundedRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(2)),
                            ),
                          ),
                          onPressed: showBottomModel,
                          child: const Text('สั่งซื้อ'),
                        ),
                      )
                    ],
                  ),
                  Container(
                    padding: const EdgeInsets.fromLTRB(10, 20, 10, 0),
                    child: Text('คำอธิบาย: ${good.description!}'),
                  ),
                ],
              ),
      ),
    );
  }
}
