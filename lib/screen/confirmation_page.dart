import 'package:flutter/material.dart';
import 'package:mono_product/model/contact.dart';
import 'package:mono_product/model/goods.dart';
import 'package:mono_product/screen/detail_page.dart';
import 'package:mono_product/widgets/background_text.dart';

class ConfirmationPage extends StatelessWidget {
  final Goods? goods;
  final Contact? contact;
  final String? payChoice;
  final int? amount;
  const ConfirmationPage(
      {Key? key, this.goods, this.contact, this.payChoice, this.amount})
      : super(key: key);

  void redirectToDetail(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => const DetailPage(
                title: 'Gun Shop',
              )),
    );
  }

  @override
  Widget build(BuildContext context) {
    int? am = amount!;
    int? price = goods!.price as int?;
    int total = (price! * am);
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              centerTitle: true,
              leading: IconButton(
                key: const Key('backbutton'),
                icon: const Icon(Icons.arrow_back, color: Colors.black),
                onPressed: () => Navigator.of(context).pop(),
              ),
            ),
            body: SingleChildScrollView(
                child: Column(
              children: <Widget>[
                Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      padding: const EdgeInsets.fromLTRB(5, 20, 0, 5),
                      child: const Text(
                        'ยืนยันการสั่งซื้อ',
                        style: TextStyle(fontSize: 15),
                      ),
                    )),
                Container(
                  decoration: BoxDecoration(
                    color: Colors.grey[350],
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                        child: Text('${goods!.name!} x ${amount}'),
                      ),
                      Container(
                        padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                        child: Text('รวม $total บาท'),
                      )
                    ],
                  ),
                ),
                const Divider(
                  color: Colors.black,
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    padding: const EdgeInsets.fromLTRB(5, 20, 0, 5),
                    child: const Text(
                      'ที่อยู่ในการจัดส่ง',
                      style: TextStyle(fontSize: 15),
                    ),
                  ),
                ),
                BackgroundText(
                  value: contact!.address!,
                ),
                Container(
                  margin: const EdgeInsets.only(top: 5.0),
                  child: BackgroundText(
                    text: 'ชื่อ',
                    value: contact!.name!,
                  ),
                ),
                Container(
                  margin: const EdgeInsets.only(top: 5.0),
                  child: BackgroundText(
                    text: 'เบอร์โทร',
                    value: contact!.phone!,
                  ),
                ),
                const Divider(
                  color: Colors.black,
                ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    padding: const EdgeInsets.fromLTRB(5, 20, 0, 5),
                    child: const Text(
                      'ช่องทางการชำระเงิน',
                      style: TextStyle(fontSize: 15),
                    ),
                  ),
                ),
                BackgroundText(
                  value: payChoice,
                ),
                const Divider(
                  color: Colors.black,
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: Container(
                    padding: const EdgeInsets.fromLTRB(0, 20, 20, 20),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(2)),
                        ),
                      ),
                      onPressed: () => redirectToDetail(context),
                      child: const Text('ยืนยัน'),
                    ),
                  ),
                )
              ],
            ))));
  }
}
