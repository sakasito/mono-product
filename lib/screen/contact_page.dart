import 'package:flutter/material.dart';
import 'package:mono_product/model/contact.dart';
import 'package:mono_product/model/goods.dart';
import 'package:mono_product/screen/pay_choice.dart';

class ContactPage extends StatefulWidget {
  final Goods? goods;
  final int? amount;
  const ContactPage({Key? key, this.goods,  this.amount}) : super(key: key);

  @override
  _ContactPageState createState() => _ContactPageState();
}

class _ContactPageState extends State<ContactPage> {
  late String name = '';
  late String phone = '';
  late String address = '';
  void redirectToPayChoice(BuildContext context) {
    final contact = Contact(name: name, phone: phone, address: address);
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => PayChoicePage(
                goods: widget.goods,
                contact: contact,
                amount: widget.amount
              )),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              centerTitle: true,
              leading: IconButton(
                key: const Key('backbutton'),
                icon: const Icon(Icons.arrow_back, color: Colors.black),
                onPressed: () => Navigator.of(context).pop(),
              ),
            ),
            body: SingleChildScrollView(
                child: Column(
              children: <Widget>[
                Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      padding: const EdgeInsets.fromLTRB(5, 20, 0, 5),
                      child: const Text(
                        'ติดต่อ',
                        style: TextStyle(fontSize: 15),
                      ),
                    )),
                Container(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                  child: TextField(
                    onChanged: (value) {
                      setState(() {
                        name = value;
                      });
                    },
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'ชื่อ-นามสกุล',
                    ),
                  ),
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                  child: TextField(
                    onChanged: (value) {
                      setState(() {
                        phone = value;
                      });
                    },
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'โทรศัพท์',
                    ),
                  ),
                ),
                Align(
                    alignment: Alignment.centerLeft,
                    child: Container(
                      padding: const EdgeInsets.fromLTRB(5, 20, 0, 5),
                      child: const Text(
                        'ที่อยู่',
                        style: TextStyle(fontSize: 15),
                      ),
                    )),
                Container(
                  padding: const EdgeInsets.fromLTRB(10, 10, 10, 0),
                  child: TextFormField(
                    onChanged: (value) {
                      setState(() {
                        address = value;
                      });
                    },
                    maxLines: 2,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'ที่อยู่',
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: Container(
                    padding: const EdgeInsets.fromLTRB(0, 20, 20, 20),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(2)),
                        ),
                      ),
                      onPressed:
                          address.isEmpty || name.isEmpty || phone.isEmpty
                              ? null
                              : () => redirectToPayChoice(context),
                      child: const Text('ยืนยัน'),
                    ),
                  ),
                )
              ],
            ))));
  }
}
