import 'package:flutter/material.dart';
import 'package:mono_product/model/contact.dart';
import 'package:mono_product/model/goods.dart';
import 'package:mono_product/screen/confirmation_page.dart';

class PayChoicePage extends StatefulWidget {
  final Goods? goods;
  final Contact? contact;
  final int? amount;
  const PayChoicePage(
      {Key? key, required this.contact, this.goods, this.amount})
      : super(key: key);

  @override
  _PayChoicePageState createState() => _PayChoicePageState();
}

class _PayChoicePageState extends State<PayChoicePage> {
  late bool isDebit = false;
  late bool isATM = false;
  late bool isTranfer = false;
  late bool isSelected = false;

  void setCheckBox(String choice) {
    if (choice == 'debit') {
      setState(() {
        isDebit = true;
        isATM = false;
        isTranfer = false;
      });
    }
    if (choice == 'ATM') {
      setState(() {
        isDebit = false;
        isATM = true;
        isTranfer = false;
      });
    }
    if (choice == 'tranfer') {
      setState(() {
        isDebit = false;
        isATM = false;
        isTranfer = true;
      });
    }
    setState(() {
      isSelected = true;
    });
  }

  void redirectToPayChoice(BuildContext context) {
    String payChoice = '';
    if (isDebit) {
      payChoice = 'บัตรเดบิต/บัตรเครดิต';
    }
    if (isATM) {
      payChoice = 'ชำระผ่าน ATM';
    }
    if (isTranfer) {
      payChoice = 'โอน/ชำระผ่านบัญชีธนาคาร';
    }
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => ConfirmationPage(
              goods: widget.goods,
              contact: widget.contact,
              payChoice: payChoice,
              amount: widget.amount)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: AppBar(
            centerTitle: true,
            leading: IconButton(
              key: const Key('backbutton'),
              icon: const Icon(Icons.arrow_back, color: Colors.black),
              onPressed: () => Navigator.of(context).pop(),
            ),
          ),
          body: SingleChildScrollView(
              child: Column(
            children: <Widget>[
              Align(
                  alignment: Alignment.centerLeft,
                  child: Container(
                    padding: const EdgeInsets.fromLTRB(5, 20, 0, 5),
                    child: const Text(
                      'การชำระเงิน',
                      style: TextStyle(fontSize: 15),
                    ),
                  )),
              Container(
                margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4.0),
                  color: Colors.grey[350],
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                        padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                        child: const Text('บัตรเดบิต/บัตรเครดิต')),
                    Container(
                      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: Checkbox(
                        value: isDebit,
                        onChanged: (bool? value) {
                          setCheckBox('debit');
                        },
                      ),
                    )
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.fromLTRB(10, 5, 10, 0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4.0),
                  color: Colors.grey[350],
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                        padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                        child: const Text('ชำระผ่าน ATM')),
                    Container(
                      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: Checkbox(
                        value: isATM,
                        onChanged: (bool? value) {
                          setCheckBox('ATM');
                        },
                      ),
                    )
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.fromLTRB(10, 5, 10, 0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4.0),
                  color: Colors.grey[350],
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                        padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                        child: const Text('โอน/ชำระผ่านบัญชีธนาคาร')),
                    Container(
                      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: Checkbox(
                        value: isTranfer,
                        onChanged: (bool? value) {
                          setCheckBox('tranfer');
                        },
                      ),
                    )
                  ],
                ),
              ),
              Align(
                alignment: Alignment.centerRight,
                child: Container(
                  padding: const EdgeInsets.fromLTRB(0, 20, 20, 20),
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      shape: const RoundedRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(2)),
                      ),
                    ),
                    onPressed:
                        !isSelected ? null : () => redirectToPayChoice(context),
                    child: const Text('ยืนยัน'),
                  ),
                ),
              )
            ],
          ))),
    );
  }
}
