import 'package:flutter/material.dart';

class BackgroundText extends StatelessWidget {
  final String? text;
  final String? value;
  final double? margin;
  const BackgroundText({
    Key? key,
    this.text,
    this.value, this.margin,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.grey[350],
      ),
      child: Row(
        children: <Widget>[
          Align(
            alignment: Alignment.centerLeft,
            child: text == null
                ? Container(
                    padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                    child: Text('$value'))
                : Container(
                    padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
                    child: Text('$text:' ' $value'),
                  ),
          ),
        ],
      ),
    );
  }
}
